'''
Created on 2018/09/13

@author: naoki_okada
'''
from abc import ABCMeta, abstractproperty, abstractmethod

class PersonAbstract(metaclass=ABCMeta):
    '''
    人間の抽象クラス
    抽象クラスは、インスタンス化できない
    概念をまとめた、基本的な処理を定義して
    継承するクラスの挙動を統一する
    '''

    def __init__(self):
        self._first_name    = None
        self._family_name = None
        self._full_name      = None

    #名を設定する
    @abstractproperty
    @abstractmethod
    def first_name(self):
        pass

    @first_name.setter
    @abstractmethod
    def first_name(self, value: str):
        pass

    @first_name.deleter
    @abstractmethod
    def first_name(self):
        pass

    #氏を設定する
    @abstractproperty
    @abstractmethod
    def family_name(self):
        pass

    @family_name.setter
    @abstractmethod
    def family_name(self, value: str):
        pass

    @first_name.deleter
    @abstractmethod
    def first_name(self):
        pass

    #フルネームを返す
    def get_full_name(self):
        pass

    #挨拶をする
    def say_hello(self):
        pass

    #さよならを言う
    def say_goodby(self):
        pass