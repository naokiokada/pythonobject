'''
Created on 2018/09/14
@author: naoki_okada
'''
class SingleSample():
    ''' サンプルの単体クラス'''

    def __init__(self):
        '''コンストラクタ '''
        self._first_name    = None
        self._family_name = None
        self._full_name      = None

    @property
    def first_name(self):
        #getter
        #プロパティにアクセスする
        return self._first_name

    @first_name.setter
    def first_name(self, value: str):
        #setter
        #プロパティをセットする
        self._first_name = value

    @first_name.deleter
    def first_name(self):
        #deleter
        #プロパティを削除
        del self._first_name

    @property
    def family_name(self):
        #getter
        #プロパティにアクセスする
        return self._family_name

    @family_name.setter
    def family_name(self, value: str):
        #setter
        #プロパティをセットする
        self._family_name = value

    @family_name.deleter
    def family_name(self):
        #deleter
        #プロパティを削除
        del self._family_name

    def get_full_name(self):
        ''' フルネームを取得する '''
        self._full_name = str(self._family_name)+str(self._first_name)

        return self._full_name