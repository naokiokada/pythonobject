'''
Created on 2018/09/14

人物情報モジュール

@author: naoki_okada
'''

''' フルネームを取得する'''
def get_full_name(first_name: str='', family_name: str='') -> str:
    return str(family_name) + str(first_name)

''' アメリカ人も考慮
　　人種が増えたらどうしよう！
'''
def get_full_name2(flg: int =1, first_name: str='', family_name: str='') -> str:
    full_name = ''
    if(flg == 1):
        #日本人の場合
        full_name = str(family_name)+str(first_name)
    elif(flg == 2):
        #アメリカ人など
        full_name = str(first_name)+str(family_name)
    else:
        pass

    return full_name