# -*- coding: utf-8 -*-

'''
以下にpythonで処理を書く
'''
import os,sys

''' フルネームを取得する '''
def get_full_name(first_name, family_name):
    return str(family_name) + str(first_name)

if __name__ == '__main__':
    first_name    = '直己'
    family_name = '岡田'
    full_name = get_full_name(first_name, family_name)
    print(full_name)